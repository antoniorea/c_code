#include <iostream>
#include <format>
 
int main() {
    std::cout << std::format("Hello {}!! \n", "world");
    std::cout << std::format("My name is {0}. I love {1}. \n", "Ali", "Educative");
}