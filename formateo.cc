// format02.cc -- function with ostream & parameter
#include <iostream>
#include <iomanip>

int main()
{

    std::ios state(nullptr);
    state.copyfmt(std::cout); // save current formatting            
             
        int a{16}, b{66};
        std::cout << std::setw(5) << a << std::setw(5) << b << std::endl;
        std::cout << std::left << std::setw(5) << a << std::setw(5) << b << std::endl;
        std::cout << " a = " << std::setbase(16) << std::setw(6) << std::showbase << a
                  << " b = " << std::setw(6) << b << std::endl;
        std::cout << std::setw(10) << a << std::setw(10) << b << std::endl;

    std::cout.copyfmt(state); // restore previous formatting   

   std::cout << std::endl;
   std::cout << std::endl;

    state.copyfmt(std::cout); // save current formatting
        double x{8.74965}, y{66.998853};
        std::cout   << std::fixed << std::setprecision(2)
                    << std::setw(6) << x << std::setw(6) << y << std::endl;
        std::cout   << std::left << std::setw(5) << x << std::setw(5) << y << std::endl;
        std::cout   << " x = " << std::setw(6)  << x
                    << " y = " << std::setw(6) << y << std::endl;
        std::cout   << std::setw(10) << x << std::setw(10) << y << std::endl;

    std::cout.copyfmt(state); // restore previous formatting
    return 0;
}