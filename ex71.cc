// ex71.cc -- calculo de la media harmonica
#include <iostream>
#include <iomanip>

double harmonic_mean(double  ,double );         // calculo de la media harmonica

int main()
{
double x{}, y{};

std::cout << "Enter two numbers ( 0 to exit):";
while( (std::cin  >> x >> y) && x != 0 && y != 0)
{
    std::cout << std::fixed << std::setprecision(2)
    << harmonic_mean(x , y) << std::setw(5) <<" is  the harmonic mean of " << x << std::setw(5)<< " and "
    << y << std::setw(5)<< std::endl;
    std::cout << "Enter two new numbers please ( 0 to exit):";

}
std::cout << "By..." << std::endl;
    return 0;
}

double harmonic_mean(double num1 ,double num2)          // calculo de la media harmonica
{
    return ( 2.0 * num1 * num2 ) / (num1 + num2);
}