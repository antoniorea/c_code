//  vectores.cc - STL Jesus Code YouTube
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

void display_vect(std::vector<int>  ve);
int main()
{    
setlocale(LC_ALL, "es_ES.UTF-8");  //para poder usar caracteres del español
std::vector<int> vi(6);

display_vect(vi);

for (auto& x: vi)   // use a reference if loop alters contents
    x = std::rand() / 1000000;

display_vect(vi);

	return 0;
}

void display_vect(std::vector<int>  ve)
{
for(auto elem : ve)
    std::cout << elem << "\t";
std::cout << std::endl; 
}