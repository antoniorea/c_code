// filefunc.cpp -- function with ostream & parameter
#include <iostream>
#include <fstream>
#include <iomanip>
//using namespace std;

void file_it(std::ostream& os, double fo, const double fe[], int n);
const int LIMIT = 5;
int main()
{
    std::ofstream fout;
    const char* fn = "ep-data.txt";
    fout.open(fn);
    if (!fout.is_open())
    {
        std::cout << "Can't open " << fn << " Bye.\n";
        exit(EXIT_FAILURE);
    }
    double objective;
    std::cout << "Enter the focal length of your "
            "telescope objective in mm: ";
    std::cin >> objective;
    double eps[LIMIT];
    std::cout << "Enter the focal lengths, in mm, of " << LIMIT
         << " eyepieces:\n";
    for (int i = 0; i < LIMIT; i++)
    {
        std::cout << "Eyepiece #" << i + 1 << ": ";
        std::cin >> eps[i];
    }
    file_it(fout, objective, eps, LIMIT);
    file_it(std::cout, objective, eps, LIMIT);
    std::cout << "Done\n";
    return 0;
}
void file_it(std::ostream& os, double fo, const double fe[], int n)
{
    std::ios state(nullptr);
    state.copyfmt(std::cout); // save current formatting 
    std::fixed;   
    std::setprecision(0);
    os << "Focal length of objective: " << fo << "mm\n"    
    << std::showpoint
    << std::fixed << std::setprecision(1);
    
    os << std::setw(12) << "f.l. eyepiece";    
    os << std::setw(15) << "magnification" << std::endl;
    for (int i = 0; i < n; i++)
    {       
        os << std::setw(12)
        << fe[i];       
        os << std::setw(15)        
        << int (fo/fe[i] + 0.5) << std::endl;
    }
    std::cout.copyfmt(state); // restore previous formatting
}
