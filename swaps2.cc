// swaps.cc -- swapping with references 
#include <iostream>
#include <iomanip>

// function template prototype
template < typename T >       // or class T
void swap(T& a, T& b);   // a, b are aliases 

int main()
{
    
    int wallet1 = 300;
    int wallet2 = 350;
    std::cout << "Using integers :\n";
    std::cout << "wallet1 = $" << std::setw(6) << wallet1;
    std::cout << "\twallet2 = $" << std::setw(6) << wallet2 << std::endl;

    std::cout << "swapping  :\n";
    swap(wallet1, wallet2);    // pass variable
    std::cout << "wallet1 = $" << std::setw(6) << wallet1;
    std::cout << "\twallet2 = $" << std::setw(6) << wallet2 << std::endl;

    double wallet3 = 300.1689;
    double wallet4 = 350.2933;
    std::ios state(nullptr);
    state.copyfmt(std::cout); // save current formatting 
    std::fixed;   
    std::setprecision(2);
        std::cout << "Using decimals:\n";
        std::cout << "wallet3 = $" << std::setw(8) << wallet3;
        std::cout << "\twallet4 = $" << std::setw(8) << wallet4 << std::endl;

        std::cout << "swapping:\n";
        swap(wallet3, wallet4);  // pass addresses of variables
        std::cout << "wallet3 = $" << std::setw(8) << wallet3;
        std::cout << "\twallet4 = $" << std::setw(8) << wallet4 << std::endl;
    std::cout.copyfmt(state); // restore previous formatting
    return 0;
}

template < typename T >
void swap(T& a, T& b)    // use references
{
    T temp;
    temp = a;       // use a, b for values of variables
    a = b;
    b = temp;
}


